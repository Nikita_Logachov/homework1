package com.company;

public class Food implements Meal {
    private int nutritional = 10;
    private int weight = 10;

    Food(int nutritional, int weight) {
        this.nutritional = nutritional;
        this.weight = weight;
    }

    @Override
    public int getNutritional() {
        return 0 + (int) (Math.random() * nutritional);
    }

    @Override
    public int getWeight() {
        return weight;
    }
}
