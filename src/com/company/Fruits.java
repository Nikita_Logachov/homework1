package com.company;

public class Fruits extends Food {

    private String name;

    Fruits(String name, int nutritional, int weight) {
        super(nutritional, weight);
        this.name = name;
    }

    public void toEat() {
        System.out.println("you ate " + name);
        System.out.println("nutritional = " + this.getNutritional());
        System.out.println("weight = " + this.getWeight());

    }

}
